/*
This file is part of Slime Volley.

	Slime Volley is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Slime Volley is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. �See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Slime Volley. �If not, see <http://www.gnu.org/licenses/>.

Copyright (c) MCMic, VinDuv.

$Id: menu_jeu.h 270 2008-12-13 21:46:13Z mcmic $
*/

#ifndef _MENU_JEU_H
#define _MENU_JEU_H 1

void menu_jeu(void);

#endif
